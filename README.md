# Supporting materials for Computability in Europe 2024 article submission.

## Overview

This repository contains supporting materials for the article titled *An optimal interpreter for approximating Kolmogorov prefix complexity* by Zoe Leyva-Acosta, Eduardo Acuña Yeomans, and Francisco Hernandez Quiroz.

## Contents

The file `imp2-complexity-estimations.csv` contains algorithmic prefix complexity estimations for binary strings.
These estimations were generated using both the Coding Theorem Method (CTM) and Smallest Program Found (SPF) approaches, calculated with an additively optimal prefix-free interpreter for the IMP2 programming language.

Columns:
- `Output`: Output of IMP2 programs as a string of bits
- `Frequency`: Programs output frequency distribution
- `CTM`: Complexity estimation using Coding Theorem Method
- `SPF`: Complexity estimation using Smallest Program Found
